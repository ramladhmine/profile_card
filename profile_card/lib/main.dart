import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profile Card',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const ProfileHomePage(),
    );
  }
}


class ProfileHomePage extends StatelessWidget {

  const ProfileHomePage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile Card"),
        centerTitle: false,
      ),
      body: Container(
          alignment: Alignment.center,
          child: Stack(
              clipBehavior: Clip.none,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10.0),
                  color: Colors.pink,
                  width: 270.0,
                  height: 170.0,
                  child: Center(
                    child: Column(
                      children: [
                      Container(
                      width: 0,
                      height: 70,
                    ),
                    Container(
                      child: Text("Bachar Rima",
                          textAlign: TextAlign.center,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            color: Colors.white,
                          )),
                    ),
                    Container(
                      child: Text("rima@lirmm.fr",
                          textAlign: TextAlign.center,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            color: Colors.white,
                          )),
                    ),
                    Container(
                      child: Text("twitter: xxxx",
                          textAlign: TextAlign.center,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            color: Colors.white,
                          )),
                    ),

                    ]
                    )
                  ),
                ),


          Positioned(
            bottom: 130,
            right: 80,
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/sun-photo.jpg'),
                    fit: BoxFit.fill,
                  ),
                  border: Border.all(
                    color: Colors.pink,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(150)),
              ),
              width: 120,
              height: 120,
            )
          )
              ] ),
      ),
    );
  }
}

